<?php

/**
 * Form for settings page
 */
function binet_perelink_admin_settings() {
  $form = array();

  $types = node_type_get_types();
  $options = array();
  foreach ($types as $type) {
    $options[$type->type] = $type->name;
  }
  $form['binet_perelink_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#description' => t('Enable perelink only to the following node types.'),
    '#default_value' => variable_get('binet_perelink_nodetypes', array()),
    '#options' => $options,
  );

  $form['binet_perelink_default_api_domain'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use default API domain'),
    '#description' => t('You should uncheck this box if API domain changed. Don\'t forget to change API domain setting in this case'),
    '#default_value' => variable_get('binet_perelink_default_api_domain', TRUE),
  );
  
  $form['binet_perelink_api_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('API domain'),
    '#description' => t('Domain for API requests to perelink service.'),
    '#default_value' => variable_get('binet_perelink_api_domain', 'http://perelink.binet.pro'),
    '#states' => array(
      'visible' => array(
        ':input[name="binet_perelink_default_api_domain"]' => array('checked' => FALSE),
      ),
      'required' => array(
        ':input[name="binet_perelink_default_api_domain"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['binet_perelink_use_api_key'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use secure API key'),
    '#description' => t('You should check this box if you using secure API key.'),
    '#default_value' => variable_get('binet_perelink_use_api_key', FALSE),
  );

  $form['binet_perelink_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('Secure API key for requests to perelink service.'),
    '#default_value' => variable_get('binet_perelink_api_key', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="binet_perelink_use_api_key"]' => array('checked' => TRUE),
      ),
      'required' => array(
        ':input[name="binet_perelink_use_api_key"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['binet_perelink_cache_time'] = array(
    '#type' => 'select',
    '#title' => t('Cache time'),
    '#description' => t('Select cache time for perelink.'),
    '#default_value' => variable_get('binet_perelink_cache_time', '300'),
    '#options' => _binet_perelink_get_cache_time(),
  );

  $form['settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 50,
  );
  
  // Block settings.
  $form['block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block'),
    '#group' => 'settings',
  );

  $form['block']['binet_perelink_in_block_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of links in block'),
    '#default_value' => variable_get('binet_perelink_in_block_limit', 3),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t("The maximum number of links to display in the block. Use 0 to turn off limit."),
    '#element_validate' => array('_binet_perelink_validate_limit'),
    '#required' => TRUE,
  );

  $form['block']['binet_perelink_capitalize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Capitalize'),
    //'#size' => 60,
    '#description' => t('Capitalize first letter of keyword.'),
    '#default_value' => variable_get('binet_perelink_capitalize', TRUE),
  );

  $form['block']['binet_perelink_default_resources'] = array(
    '#type' => 'checkbox',
    '#title' => t('Attach module css and js'),
    '#description' => t('Use this checkbox to attach default css and js provided by module'),
    '#default_value' => variable_get('binet_perelink_default_resources', TRUE),
  );

  $form['block']['binet_perelink_type'] = array(
    '#type' => 'select',
    '#title' => t('Perelink type'),
    '#description' => t('Which type of perelink you prefer.'),
    '#default_value' => variable_get('binet_perelink_type', 'text'),
    '#options' => array('text' => t('Text only'), 'thumb' => t('Text with thumb')),
  );

  $form['block']['binet_perelink_image_field'] = array(
    '#type' => 'select',
    '#title' => t('Image field'),
    '#description' => t('Select image field for perelink thumbs.'),
    '#default_value' => variable_get('binet_perelink_image_field', ''),
    '#options' => _binet_perelink_get_image_fields(),
    '#states' => array(
      'visible' => array(
        'select[name="binet_perelink_type"]' => array('value' => 'thumb'),
      ),
      'required' => array(
        'select[name="binet_perelink_type"]' => array('value' => 'thumb'),
      ),
    ),
  );

  $form['block']['binet_perelink_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Image style'),
    '#description' => t('Image style preset for perelink thumbs.'),
    '#default_value' => variable_get('binet_perelink_image_style', ''),
    '#options' => image_style_options(),
    '#states' => array(
      'visible' => array(
        'select[name="binet_perelink_type"]' => array('value' => 'thumb'),
      ),
      'required' => array(
        'select[name="binet_perelink_type"]' => array('value' => 'thumb'),
      ),
    ),
  );

  // Text settings.
  $form['text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Text'),
    '#group' => 'settings',
  );

  $form['text']['binet_perelink_in_text_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of links in text'),
    '#default_value' => variable_get('binet_perelink_in_text_limit', 5),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t("The maximum number of links to display in the text. Use 0 to turn off limit."),
    '#element_validate' => array('_binet_perelink_validate_limit'),
    '#required' => TRUE,
  );
  
  $form['text']['binet_perelink_in_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable in-text pererlink'),
    //'#size' => 60,
    '#description' => t('Allow to use perelink in the text.'),
    '#default_value' => variable_get('binet_perelink_in_text', TRUE),
  );

  $form['#submit'][] = '_binet_perelink_admin_settings_submit';
  $form['#validate'][] = '_binet_perelink_admin_settings_validate';
  return system_settings_form($form);
}

/**
 * We should clear caches after form submissions.
 */
function _binet_perelink_admin_settings_submit($form, &$form_state) {
  drupal_flush_all_caches();
  //watchdog('perelink', 'Cache cleared');
}

/**
 * Make sure we selected options for Image field & Image style, when perelink type is Text with thumb.
 */
function _binet_perelink_admin_settings_validate($form, $form_state){
  if($form_state['values']['binet_perelink_type'] == 'thumb'){
    if(empty($form_state['values']['binet_perelink_image_field'])) form_set_error('binet_perelink_image_field', t('Please select value for Image field.'));
    if(empty($form_state['values']['binet_perelink_image_style'])) form_set_error('binet_perelink_image_style', t('Please select value for Image style.'));
  }
  if(!$form_state['values']['binet_perelink_default_api_domain']){
    if(empty($form_state['values']['binet_perelink_api_domain'])) form_set_error('binet_perelink_api_domain', t('Please enter value for API domain field.'));
  }
}

function _binet_perelink_validate_limit($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive integer or zero.', array('%name' => $element['#title'])));
  }
}

function _binet_perelink_get_image_fields(){
  $result = array('' => t('<none>'));
  $fields = field_info_field_map();
  foreach($fields as $key => $field){
    if($field['type'] == 'image') $result[$key] = $key;
  }
  return $result;
}

function _binet_perelink_get_cache_time(){
  $result = array(
    '300' => format_plural(5, '1 minute', '@count minutes'),
    '600' => format_plural(10, '1 minute', '@count minutes'),
    '1800' => format_plural(30, '1 minute', '@count minutes'),
    '3600' => format_plural(1, '1 hour', '@count hours'),
    '21600' => format_plural(6, '1 hour', '@count hours'),
    '43200' => format_plural(12, '1 hour', '@count hours'),
    '86400' => format_plural(1, '1 day', '@count days'),
    '604800' => format_plural(1, '1 week', '@count weeks'),
    '1209600' => format_plural(2, '1 week', '@count weeks'),
  );
  
  return $result;
}